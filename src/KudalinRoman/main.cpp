#include <fstream>
#include "Interface_Employee.h"
#include "Personal.h"
#include "Engineer.h"
#include "Manager.h"
using namespace std;

const int NUMBER_OF_EMPLOYEES = 41;
const int DEFAULT_WORKTIME = 50;

#define SAVE_TO_FILE

int toInt(string& str);

double toDouble(string& str);

int main()
{
	Employee *staff[NUMBER_OF_EMPLOYEES];
	int i = 0;
	ifstream dataFile("data.txt");
	if (!dataFile.is_open())
	{
		cout << "Error while opening file \"data.txt\"" << endl;
		exit(EXIT_FAILURE);
	}
	string curID, curName, curPosition, curWorkTime, curPayment, temp;
	while (!dataFile.eof())
	{
		getline(dataFile, curID);
		getline(dataFile, curName);
		getline(dataFile, curPosition);
		getline(dataFile, curWorkTime);
		getline(dataFile, curPayment);
		if (curPosition == "Driver")
		{
			string curBase;
			getline(dataFile, curBase);
			staff[i++] = new Driver(toInt(curID), curName, toInt(curWorkTime), toDouble(curPayment), toInt(curBase));
			getline(dataFile, temp);
			continue;
		}
		if (curPosition == "Cleaner")
		{
			string curBase;
			getline(dataFile, curBase);
			staff[i++] = new Cleaner(toInt(curID), curName, toInt(curWorkTime), toDouble(curPayment), toInt(curBase));
			getline(dataFile, temp);
			continue;
		}
		if (curPosition == "Programmer")
		{
			string curBase, projTitle, part, projBudg;
			getline(dataFile, projTitle);
			getline(dataFile, part);
			getline(dataFile, curBase);
			getline(dataFile, projBudg);
			staff[i++] = new Programmer(toInt(curID), curName, toInt(curWorkTime), toDouble(curPayment), projTitle, 
										toDouble(part), toInt(curBase), toInt(projBudg));
			getline(dataFile, temp);
			continue;
		}
		if (curPosition == "Tester")
		{
			string curBase, projTitle, part, projBudg;
			getline(dataFile, projTitle);
			getline(dataFile, part);
			getline(dataFile, curBase);
			getline(dataFile, projBudg);
			staff[i++] = new Tester(toInt(curID), curName, toInt(curWorkTime), toDouble(curPayment), projTitle, 
									toDouble(part), toInt(curBase), toInt(projBudg));
			getline(dataFile, temp);
			continue;
		}
		if (curPosition == "TeamLeader")
		{
			string curBase, projTitle, part, projBudg, subNumber, feeForOne;
			getline(dataFile, projTitle);
			getline(dataFile, part);
			getline(dataFile, curBase);
			getline(dataFile, projBudg);
			getline(dataFile, subNumber);
			getline(dataFile, feeForOne);
			staff[i++] = new TeamLeader(toInt(curID), curName, toInt(curWorkTime), toDouble(curPayment), projTitle, 
										toDouble(part), toInt(curBase), toInt(projBudg), toInt(subNumber), toInt(feeForOne));
			getline(dataFile, temp);
			continue;
		}
		if (curPosition == "Manager")
		{
			string curBase, projTitle, part, projBudg;
			getline(dataFile, projTitle);
			getline(dataFile, part);
			getline(dataFile, projBudg);
			staff[i++] = new Manager(toInt(curID), curName, toInt(curWorkTime), toDouble(curPayment), projTitle,
				toDouble(part), toInt(projBudg));
			getline(dataFile, temp);
			continue;
		}

		if (curPosition == "ProjectManager")
		{
			string projTitle, part, projBudg, subNumber, feeForOne;
			getline(dataFile, projTitle);
			getline(dataFile, part);
			getline(dataFile, projBudg);
			getline(dataFile, subNumber);
			getline(dataFile, feeForOne);
			staff[i++] = new ProjectManager(toInt(curID), curName, toInt(curWorkTime), toDouble(curPayment), projTitle, 
											toDouble(part), toInt(projBudg), toInt(subNumber), toInt(feeForOne));
			getline(dataFile, temp);
			continue;
		}
		if (curPosition == "SeniorManager")
		{
			string projTitle, part, projBudg, subNumber, feeForOne, projNum;
			getline(dataFile, projTitle);
			getline(dataFile, part);
			getline(dataFile, projBudg);
			getline(dataFile, subNumber);
			getline(dataFile, feeForOne);
			getline(dataFile, projNum);
			staff[i++] = new SeniorManager(toInt(curID), curName, toInt(curWorkTime), toDouble(curPayment), projTitle,
				toDouble(part), toInt(projBudg), toInt(subNumber), toInt(feeForOne), toInt(projNum));
			getline(dataFile, temp);
			continue;
		}
	}
	dataFile.close();

	ofstream data_out("data_out.txt");

	for (i = 0; i < NUMBER_OF_EMPLOYEES; i++)
	{
		staff[i]->setWorkTime(DEFAULT_WORKTIME);
		staff[i]->calc();

		#ifdef SAVE_TO_FILE
			if (!data_out.is_open())
			{
				cout << "Error while creating file \"data_out.txt\"" << endl;
				exit(EXIT_FAILURE);
			}
			staff[i]->showInfo(data_out);
			cout << "All changes have been saved. Check \"data_out.txt\"" << endl;
		#else
			staff[i]->showInfo(cout);
		#endif
	}
	data_out.close();
	return 0;
}

int toInt(string& str)
{
	return atoi(str.c_str());
}

double toDouble(string& str)
{
	return atof(str.c_str());
}